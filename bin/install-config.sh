#!/bin/bash
COMMAND=$1

# Exit if not passed
if [ -z "$COMMAND" ]; then
    echo "Command not found"
    exit 1
fi

gitignore () {
    install_file ".gitignore_template" ".gitignore"
}

jest () {
    install_file "jest.config.js" "jest.config.js"
}

babel () {
    install_file "babel.config.js" "babel.config.js"
}

prettier () {
    install_file ".prettierrc.js" ".prettierrc.js"
}

eslint () {
    install_file ".eslintrc.json" ".eslintrc.json"
}

webpack_sls () {
    install_file "serverless.webpack.config.js" "webpack.config.js"
}

all () {
    gitignore
    jest
    babel
    prettier
    eslint
    webpack_sls
}

install_file () {
    SOURCE_FILE_NAME=$1
    DEST_FILE_NAME=$2

    CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

    SOURCE_FILE_PATH="$CURRENT_DIR/configs/$SOURCE_FILE_NAME"
    DEST_FILE_PATH="./$DEST_FILE_NAME"

    cp $SOURCE_FILE_PATH $DEST_FILE_PATH
}

# Run
$($COMMAND)
/* eslint-disable import/no-commonjs */
const path = require("path");
// const nodeExternals = require("webpack-node-externals");
const slsw = require("serverless-webpack");

module.exports = {
    entry: slsw.lib.entries,
    target: "node",
    // externals: [nodeExternals()],
    module: {
        rules: [
            {
                test: /\.js$/,
                loaders: ["babel-loader"],
                include: __dirname
            }
        ]
    },
    output: {
        libraryTarget: "commonjs",
        path: path.join(__dirname, ".webpack"),
        filename: "[name].js"
    }
};

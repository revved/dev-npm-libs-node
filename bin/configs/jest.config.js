// eslint-disable-next-line import/no-commonjs
module.exports = {
    // Stop running tests after `n` failures
    bail: 3,
    // Automatically clear mock calls and instances between every test
    clearMocks: true,
    // Indicates whether the coverage information should be collected while executing the test
    collectCoverage: true,
    // The directory where Jest should output its coverage files
    coverageDirectory: "coverage",
    // A list of reporter names that Jest uses when writing coverage reports
    coverageReporters: ["text", "lcov"],

    // An object that configures minimum threshold enforcement for coverage results
    coverageThreshold: {
        global: {
            branches: 95,
            functions: 95,
            lines: 95,
            statements: 95
        }
    },
    // The test environment that will be used for testing
    testEnvironment: "node",
    verbose: true
};
